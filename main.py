import argparse
from pprint import pprint

from constants import START_DATE, END_DATE, HELP_TEXT
from repo_analysis import RepoAnalysis


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="url to github repository")
    parser.add_argument(
        "-b", "--branch", type=str, default="master", help="branch to analyse"
    )
    parser.add_argument(
        "-s", "--start_date", type=str, default=START_DATE, help=f"start {HELP_TEXT}",
    )
    parser.add_argument(
        "-e", "--end_date", type=str, default=END_DATE, help=f"end {HELP_TEXT}"
    )
    parser.add_argument(
        "-t",
        "--token",
        type=str,
        default=None,
        help="token to avoid call limits up to 5000",
    )
    args = parser.parse_args()

    analysis = RepoAnalysis(**dict(args._get_kwargs()))
    analysis.get_full_analysis()
    pprint(analysis.analysis_data)


if __name__ == "__main__":
    main()
