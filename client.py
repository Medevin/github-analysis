from constants import BASE_URL
from utils import make_request, token_init


class GithubClient:
    def __init__(self, owner, repo, token):
        self.owner = owner
        self.repo = repo
        self.headers = token_init(token)

    def get_endpoint_data(self, endpoint, params):
        return make_request(
            f"{BASE_URL}/{self.owner}/{self.repo}/{endpoint}", params, self.headers
        )
