from datetime import datetime

START_DATE = "1970-01-01T00:00:00Z"
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
END_DATE = datetime.now().strftime(DATETIME_FORMAT)
HELP_TEXT = "date in ISO-8601 format (1970-01-01T00:00:00Z)"
BASE_URL = "https://api.github.com/repos"
TEMPLATE = r"github\.com/([-_\.+\w]*)/([-_\.+\w]*)/"
COMMITS_PER_PAGE = 100
ESCAPE_CHARACTERS = 6
COMMIT = 1
MAX_RESULTS = 30
PULLS_DAYS_LIMIT = 30
ISSUES_DAYS_LIMIT = 14
OPEN_STATE = "open"
CLOSED_STATE = "closed"
