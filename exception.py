class InvalidUrlException(Exception):
    def __str__(self):
        return "Invalid url format"


class RequestException(Exception):
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message

    def __str__(self):
        return f"Status code - {self.status_code},\nMessage - {self.message}"
