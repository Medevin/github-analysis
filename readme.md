## Github repo analysis 

Requirements:
- python 3.7.6
- requests 2.24.0

### Run app with python interpreter in command line:

`python3 main.py <url>`

`<url>` - is required parameter(link to github repo)

### Optional parameters with default values:

`-s` or `--start_date` - analysis start date (default - 1970-01-01T00:00:00Z)

`-e` or `--end_date`- analysis end date (default - current date)

`-b` or `--branch` - branch to analyze (default - master)

`-t` or `--token` - github token (default - None)

### Additionally:
 - all dates should be provided in ISO 8601 format, eg. 1970-01-01T00:00:00Z
 - token allow to make up to 5000 requests, if not provided - limit is 100
