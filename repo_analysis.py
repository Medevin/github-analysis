from utils import get_url_params, days_out_of_limit, date_in_range, str_to_datetime
from constants import MAX_RESULTS, OPEN_STATE, PULLS_DAYS_LIMIT, ISSUES_DAYS_LIMIT
from client import GithubClient


class RepoAnalysis:
    def __init__(self, url, branch, start_date, end_date, token):
        self.client = GithubClient(*get_url_params(url), token=token)
        self.branch = branch
        self.start_date = str_to_datetime(start_date)
        self.end_date = str_to_datetime(end_date)
        self.query_params = {
            "sha": branch,
            "since": start_date,
            "until": end_date,
            "base": branch,
            "state": "all",
            "sort": "created",
            "per_page": 30,
            "page": 0,
        }
        self.commits_data = list()
        self.pulls_data = list()
        self.issues_data = list()
        self.analysis_data = dict()

    def _get_all_pages_data(self, endpoint, data_container):
        params = self.query_params.copy()
        while True:
            params["page"] += 1
            data = self.client.get_endpoint_data(endpoint, params)
            if not data:
                break
            data_container.extend(data)

    def most_active_users(self):
        self._get_all_pages_data("commits", self.commits_data)
        users_list = [commit["author"]["login"] for commit in self.commits_data]
        users_commits = {name: users_list.count(name) for name in set(users_list)}
        self.analysis_data["users"] = [
            f"{k}: {users_commits[k]}"
            for k in sorted(users_commits, key=users_commits.get, reverse=True)[
                :MAX_RESULTS
            ]
        ]

    def _states_count(self, endpoint, data_container, days_limit):
        self._get_all_pages_data(endpoint, data_container)
        open_closed_states = {"open": 0, "closed": 0}
        outdated_items = 0
        for pull in self.pulls_data:
            if date_in_range(self.start_date, self.end_date, pull["created_at"]):
                open_closed_states[pull["state"]] += 1
                if pull["state"] == OPEN_STATE and days_out_of_limit(
                    pull["created_at"], days_limit
                ):
                    outdated_items += 1
        self.analysis_data[f"open_closed_{endpoint}"] = [
            f"{state} state: {value}" for state, value in open_closed_states.items()
        ]
        self.analysis_data[f"outdated_{endpoint}"] = [
            f"outdated {endpoint}: {outdated_items}"
        ]

    def pulls_states(self):
        self._states_count("pulls", self.pulls_data, PULLS_DAYS_LIMIT)

    def issues_states(self):
        self._states_count("issues", self.pulls_data, ISSUES_DAYS_LIMIT)

    def get_full_analysis(self):
        self.most_active_users()
        self.pulls_states()
        self.issues_states()