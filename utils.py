import re
import requests

from constants import TEMPLATE, DATETIME_FORMAT
from datetime import datetime
from exception import InvalidUrlException, RequestException


def get_url_params(url):
    url_params = re.search(re.compile(TEMPLATE), url)
    if url_params:
        return url_params.groups()
    raise InvalidUrlException


def make_request(url, params, headers):
    with requests.Session() as s:
        response = s.get(url, params=params, headers=headers)
    if response.status_code == 200:
        return response.json()
    raise RequestException(response.status_code, response.json())


def token_init(token):
    return {"Authorization": f"token {token}"} if token else None


def str_to_datetime(date):
    try:
        return datetime.strptime(date, DATETIME_FORMAT)
    except Exception:
        print(f"{date} invalid date format, expected ISO-8601")
        raise


def days_out_of_limit(time, days_limit):
    return (datetime.now() - str_to_datetime(time)).days > days_limit


def date_in_range(start_date, end_date, check_date):
    return start_date <= str_to_datetime(check_date) <= end_date
